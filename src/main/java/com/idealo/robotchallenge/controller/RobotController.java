package com.idealo.robotchallenge.controller;

import com.idealo.robotchallenge.model.Instruction;
import com.idealo.robotchallenge.model.Robot;
import com.idealo.robotchallenge.service.RobotService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RobotController {

    private final RobotService robotService;

    public RobotController(RobotService robotService) {
        this.robotService = robotService;
    }

    /**
     * endpoint
     * @param instruction - contains information about current position&direction of the robot and predefined movements
     * {
     * "currentPositionOfRobot" : {"x": 0,"y": 0},
     * "currentDirectionOfRobot": "EAST",
     * "controls": ["POSITION 1 3 EAST", "FORWARD 3", "WAIT", "TURNAROUND", "FORWARD 1", "RIGHT", "FORWARD 2"]
     * }
     * @return Robot - robot's latest position
     */
    @PostMapping("/move")
    public Robot moveRobot(@RequestBody Instruction instruction) {
        return robotService.moveRobot(instruction);
    }

}
