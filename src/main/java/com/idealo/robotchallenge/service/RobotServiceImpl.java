package com.idealo.robotchallenge.service;

import com.idealo.robotchallenge.exception.OutOfGridException;
import com.idealo.robotchallenge.model.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Log4j2
@Service
public class RobotServiceImpl implements RobotService {

    private final ControlService controlService;

    public RobotServiceImpl(ControlService controlService) {
        this.controlService = controlService;
    }

    @Override
    public Robot moveRobot(Instruction instruction) {
        Robot robot = new Robot(instruction);
        Grid grid = Grid.defaultGrid(robot);
        if (!grid.isRobotWithinGrid()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Robot current position is outside of the grid.");
        }
        return moveRobot(instruction.getControls(), grid);
    }

    private Robot moveRobot(List<String> controlElements, Grid grid) {
        for (String controlElement : controlElements) {
            defineMovement(controlElement, grid.getRobot());
            if (!grid.isRobotWithinGrid()) {
                throw new OutOfGridException();
            }
        }
        return grid.getRobot();
    }

    private void defineMovement(String element, Robot robot) {
        String[] elements =  element.toUpperCase().split(" ");
        Movement movement = Movement.valueOf(elements[0]);

        switch (movement) {
            case POSITION:
                setPosition(elements, robot);
                break;
            case WAIT:
                controlService.wait(robot);
                break;
            case FORWARD:
                moveForward(elements, robot);
                break;
            case TURNAROUND:
                controlService.turnAround(robot);
                break;
            case RIGHT:
                controlService.moveRight(robot);
                break;
            default:
                throw new RuntimeException("Can not find a suitable control");
        }
    }

    private void setPosition(String[] elements, Robot robot) {
        if (elements.length != 4) {
            throw new IllegalArgumentException("POSITION command needs the form [POSITION x y direction]");
        }
        int x = Integer.parseInt(elements[1]);
        int y = Integer.parseInt(elements[2]);
        Direction direction = Direction.valueOf(elements[3]);
        robot.setPosition(new Position(x, y));
        robot.setDirection(direction);
    }

    private void moveForward(String[] elements, Robot robot) {
        if (elements.length != 2) {
            throw new IllegalArgumentException("FORWARD command needs the form [FORWARD steps]");
        }
        int steps = Integer.parseInt(elements[1]);
        controlService.moveForward(robot, steps);
    }
}
