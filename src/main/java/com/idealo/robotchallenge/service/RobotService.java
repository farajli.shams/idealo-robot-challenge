package com.idealo.robotchallenge.service;

import com.idealo.robotchallenge.model.Instruction;
import com.idealo.robotchallenge.model.Robot;

public interface RobotService {
    /**
     * @param instruction - contains information about current position&direction of the robot and predefined movements
     * @return Robot - robot's latest position
     */
    Robot moveRobot(Instruction instruction);
}
