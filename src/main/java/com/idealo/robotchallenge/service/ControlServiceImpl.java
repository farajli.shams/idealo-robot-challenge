package com.idealo.robotchallenge.service;

import com.idealo.robotchallenge.model.Direction;
import com.idealo.robotchallenge.model.Robot;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ControlServiceImpl implements ControlService {

    @Override
    public void wait(Robot robot) {
        log.info("Robot is waiting");
    }

    @Override
    public void moveForward(Robot robot, int steps) {
        log.info("Robot is moving " + steps + " steps forward");
        switch (robot.getDirection()) {
            case NORTH:
                robot.getPosition().addY(steps);
                break;
            case SOUTH:
                robot.getPosition().addY(steps * -1);
                break;
            case EAST:
                robot.getPosition().addX(steps);
                break;
            case WEST:
                robot.getPosition().addX(steps * -1);
                break;
        }
    }

    @Override
    public void moveRight(Robot robot) {
        log.info("Robot is moving right");
        switch (robot.getDirection()) {
            case NORTH:
                robot.setDirection(Direction.EAST);
                break;
            case EAST:
                robot.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                robot.setDirection(Direction.WEST);
                break;
            case WEST:
                robot.setDirection(Direction.NORTH);
                break;
        }
    }

    @Override
    public void turnAround(Robot robot) {
        log.info("Robot is turning around");
        switch (robot.getDirection()) {
            case NORTH:
                robot.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                robot.setDirection(Direction.NORTH);
                break;
            case EAST:
                robot.setDirection(Direction.WEST);
                break;
            case WEST:
                robot.setDirection(Direction.EAST);
                break;
        }
    }
}
