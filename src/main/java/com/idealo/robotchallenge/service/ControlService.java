package com.idealo.robotchallenge.service;

import com.idealo.robotchallenge.model.Robot;

public interface ControlService {

    void wait(Robot robot);

    void moveForward(Robot robot, int steps);

    void moveRight(Robot robot);

    void turnAround(Robot robot);
}
