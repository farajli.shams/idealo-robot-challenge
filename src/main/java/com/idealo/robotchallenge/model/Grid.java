package com.idealo.robotchallenge.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
public class Grid {
    private int maxX;
    private int maxY;

    @Getter
    @Setter
    private Robot robot;

    /**
     * creates new Grid with default coordinates and sent robot object
     */
    public static Grid defaultGrid(Robot robot) {
        return new Grid(5, 5, robot);
    }

    /**
     * checks if robot is in grid
     */
    public boolean isRobotWithinGrid() {
        if ((robot.getPosition().getX() < 0 && maxX > 0)
                || (robot.getPosition().getX() > 0 && maxX < 0)
                || (robot.getPosition().getY() < 0 && maxY > 0)
                || (robot.getPosition().getY() > 0 && maxY < 0)) {
            return false;
        }
        return Math.abs(robot.getPosition().getX()) <= Math.abs(maxX)
                && Math.abs(robot.getPosition().getY()) <= Math.abs(maxY);
    }
}
