package com.idealo.robotchallenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Instruction {
    private Position currentPositionOfRobot;
    private Direction currentDirectionOfRobot;
    private List<String> controls;
}
