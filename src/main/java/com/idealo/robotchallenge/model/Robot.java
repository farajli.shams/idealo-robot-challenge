package com.idealo.robotchallenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Robot {
    private Position position;
    private Direction direction;

    public Robot(Instruction movementInstructions) {
        this.direction = movementInstructions.getCurrentDirectionOfRobot();
        this.position = movementInstructions.getCurrentPositionOfRobot();
    }
}
