package com.idealo.robotchallenge.model;

public enum Movement {
    POSITION,
    RIGHT,
    FORWARD,
    WAIT,
    TURNAROUND
}
