package com.idealo.robotchallenge.model;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
