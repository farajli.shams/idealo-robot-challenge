package com.idealo.robotchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdealoRobotChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdealoRobotChallengeApplication.class, args);
    }

}
