package com.idealo.robotchallenge;

import com.idealo.robotchallenge.model.Direction;
import com.idealo.robotchallenge.model.Grid;
import com.idealo.robotchallenge.model.Position;
import com.idealo.robotchallenge.model.Robot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GridTest {
    private Grid testGrid;

    @Test
    void isRobotPositionWithinGrid() {
        Position testPosition = new Position(1, 3);
        Robot testData = new Robot(testPosition, Direction.EAST);
        testGrid = new Grid(5, 5, testData);
        assertTrue(testGrid.isRobotWithinGrid());
    }

    @Test
    void isRobotPositionOutsideTheGrid() {
        Position testPosition = new Position(1, 6);
        Robot testData = new Robot(testPosition, Direction.EAST);
        testGrid = new Grid(5, 5, testData);
        assertFalse(testGrid.isRobotWithinGrid());
    }
}
