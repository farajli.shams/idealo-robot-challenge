package com.idealo.robotchallenge.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RobotControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void moveRobotTestSuccess() throws Exception {
        String postBodyPayload = "{\n" +
                "\"currentPositionOfRobot\" : {\"x\": 0,\"y\": 0},\n" +
                "\"currentDirectionOfRobot\": \"EAST\",\n" +
                "\"controls\": [\"POSITION 1 3 EAST\", \"FORWARD 3\", \"WAIT\", \"TURNAROUND\", \"FORWARD 1\", \"RIGHT\", \"FORWARD 2\"]\n" +
                "}";

        String expectedResponseBody = "{\"position\":{\"x\":3,\"y\":5},\"direction\":\"NORTH\"}";

        mvc.perform(MockMvcRequestBuilders.post("/move")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postBodyPayload))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseBody));
    }

    @Test
    void moveRobotTestError() throws Exception {
        String postBodyPayload = "{\n" +
                "\"currentPositionOfRobot\" : {\"x\": 6,\"y\": 0},\n" +
                "\"currentDirectionOfRobot\": \"EAST\",\n" +
                "\"controls\": [\"POSITION 1 3 EAST\", \"FORWARD 3\", \"WAIT\", \"TURNAROUND\", \"FORWARD 1\", \"RIGHT\", \"FORWARD 2\"]\n" +
                "}";

        mvc.perform(MockMvcRequestBuilders.post("/move")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postBodyPayload))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Robot current position is outside of the grid."));
    }
}
